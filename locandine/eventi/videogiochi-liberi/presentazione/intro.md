---
marp: true
theme: uncover
---

<!-- backgroundColor: #eeeeee -->

![OpenIT Este, logo](../../../../logo/OpenIT%20Este.png)

# 🎮 Videogiochi Liberi
🕘 1 giugno 2023 - 21:00

https://este.linux.it

![Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

---

<!-- backgroundColor: #d0c5b8 -->

![Locandina w:600](https://este.linux.it/novita/videogiochi-liberi/videogiochi-liberi2.webp)

---

<!-- backgroundColor: #eeeeee -->

# 🐧 Chi siamo

![Pinguini w:800](https://este.linux.it/chi-siamo/antarctic-gce07cbed1_1280.webp)

Un gruppo Linux Veneto di Este (PD) nato a fine 2022

---

# 🚀 Cosa facciamo

- supporto per il passaggio a GNU/Linux
- alternative libere a app, programmi o servizi di uso comune
- mappatura con OpenStreetMap
- divulgazione

---

# 👨‍💻 Progetti

Sul nostro sito ci sono alcuni progetti avviati come:
- mappatura raccoglitori di olio
- Web Developer Privacy Kit: insieme di pratiche e strumenti per costruire applicazioni e siti web orientati alla privacy
- (tentativo di) aggiornamento di Este su Wikipedia

---

# 🗓️ Eventi
Trovate tutti gli eventi sul calendario, ma li potete trovare aggregati anche nel pianeta di Linux.it:

https://planet.linux.it/eventi/

---

# 🔥 Iniziamo!
![Pinguino malevolo](images/iniziamo.jpg)
