# Presentazioni

Sono scritte usando [Marp](https://marp.app/), un sistema di presentazioni basato su Markdown, così da **concentrarsi sui contenuti** e molto meno sullo strumento e la parte grafica.

## Come scrivere

Per scriverle è consigliato scaricare un file di esempio dal sito di Marp o usare come modello uno dei file markdown qui presenti. C'è un'estensione per VisualStudio chiamata [Marp for VS Code](https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode) che permette di visualizzare in anteprima le presentazioni mentre vengono scritte.

## Installazione

Occorre prima [installare Marp](https://github.com/marp-team/marp-cli#global-installation).


### Anteprima su browser

Si possono visualizzare in tempo reale su browser tramite:

```bash
marp -s <nome_cartella>
```

In questo modo, aprendo [localhost:8080](http://localhost:8080) si può navigare all'interno della cartella e avviare qualsiasi presentazione.

### Generazione file

Si possono generare dei file HTML, PDF o PPTX tramite questi comandi, praticamente basta cambiare estensione del file in uscita:

```bash
# Generazione in HTML
marp presentazione.md --no-config -o presentazione.html

# Generazione in PDF
marp presentazione.md --no-config --allow-local-file -o presentazione.pdf

# Generazione in PPTX
marp presentazione.md --no-config --allow-local-file -o presentazione.pptx
```
