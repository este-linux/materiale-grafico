# Videogiochi Liberi

Serata dedicata all'introduzione dei videogiochi liberi in collaborazione con [Etica Digitale](https://eticadigitale.org/). Ospite [Zughy](https://gitlab.com/marco_a) che proporrà alternative ai classici giochi commerciali e chiusi.


## Licenza

Opera distribuita con licenza CC BY-SA 4.0.

Contenuti derivati da:

https://gitlab.com/marco_a/minetest-flyer-poster/-/tree/main

