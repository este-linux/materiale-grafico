# Coderdojo

Incontro in presenza orientato all'insegnamento della programmazione a bambinɜ di età fra 7 e 14 anni, utilizzando principalmente [Scratch](https://scratch.mit.edu/).

## Licenza

Opera distribuita con licenza CC BY-SA 4.0.
